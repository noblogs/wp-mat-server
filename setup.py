from setuptools import setup, find_packages

setup(
	name='mat_api_server',
	version='0.1',
	description='Simple API server for MAT2',
	packages=find_packages(),
	install_requires=[
	  'Flask',
          'mat2',
	],
)
