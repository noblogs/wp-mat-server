import pathlib
import pytest
from mat_api_server.api import app as flask_app


resources = pathlib.Path(__file__).parent


@pytest.fixture()
def app():
    flask_app.config.update({
        "TESTING": True,
    })
    yield flask_app


@pytest.fixture()
def client(app):
    return app.test_client()


def test_cleanup_image(client):
    testfd = (resources / "test.jpg").open("rb")
    response = client.post(
        "/api/cleanup", data={
            "mime_type": "image/jpeg",
            "file": (testfd, "test.jpg", "image/jpeg"),
        })
    assert response.status_code == 200
    # Dirty check for "have we got a bunch of data that looks a bit
    # like a JPEG"?
    assert len(response.data) > 1024
    assert b'JFIF' in response.data
