from flask import Flask, abort, request, send_file
from libmat2 import parser_factory

import mimetypes
import os
import tempfile


app = Flask(__name__)


def get_parser_by_mime_type(path, mime_type):
    for parser_class in parser_factory._get_parsers():
        if mime_type in parser_class.mimetypes:
            return parser_class(path)


def process_file(path, mime_type):
    app.logger.info('processing file %s (%s)', path, mime_type)
    parser = get_parser_by_mime_type(path, mime_type)
    if not parser:
        app.logger.error("unsupported mime type '%s'", mime_type)
        abort(422)
    if not parser.remove_all():
        app.logger.error('failed to clean file')
        abort(422)
    return parser.output_filename


@app.route('/api/cleanup', methods=('POST',))
def cleanup():
    if 'file' not in request.files:
        abort(400)

    mime_type = request.form['mime_type']
    uploaded_file = request.files['file']
    if uploaded_file.filename == '':
        abort(400)

    # Everything (input and output) needs to be file-backed because
    # mat needs to invoke external programs. And the input extension
    # needs to be correct as well, there are several checks for it
    # in the mat2 code.
    with tempfile.NamedTemporaryFile(
            dir=app.config.get('UPLOAD_DIR'),
            suffix=mimetypes.guess_extension(mime_type)) as tmpf:
        uploaded_file.save(tmpf)
        output_file = process_file(tmpf.name, mime_type)
        try:
            return send_file(output_file)
        finally:
            os.remove(output_file)
