import os
from .api import app


# Simple config from environment variables.
app.config.update({
    'UPLOAD_DIR': os.getenv('UPLOAD_DIR'),
})


application = app
