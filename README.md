wp-mat-server
===

This is a very simple packaging of the
[mat2](https://0xacab.org/jvoisin/mat2) framework as a HTTP RPC
application. It is meant to be run as an internal trusted service, so
it differs from the default
[mat2-web](https://0xacab.org/jvoisin/mat2-web) application because it
is not centered around a user upload / user download workflow: the RPC
API of *wp-mat-server* just returns the response inline.

The service is meant to be used as the backend for the
[wp-mat](https://git.autistici.org/noblogs/wp-mat) Wordpress plugin.

### Container image

The container image built by this repository's Dockerfile is meant to
be run as a non-root user, in a highly restricted environment. As a
consequence, wp-mat-server does not use *bubblewrap* to further
sandbox mat2 command invocations. If your environment is less
controlled, this might pose a security risk.
