FROM debian:bookworm-slim AS build

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3 python3-dev python3-pip python3-setuptools python3-wheel \
        git build-essential libgirepository1.0-dev libcairo2-dev

ADD . /src
WORKDIR /src

RUN mkdir -p dist && \
    pip3 wheel -r requirements.txt -w dist && \
    python3 setup.py bdist_wheel

FROM registry.git.autistici.org/ai3/docker/uwsgi-base:master

COPY --from=build /src/dist/*.whl /tmp/wheels/
RUN cd /tmp/wheels \
    && /virtualenv/bin/pip3 install *.whl \
    && rm -fr /tmp/wheels \
    && apt-get -q update \
    && env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        libimage-exiftool-perl gobject-introspection \
        libgdk-pixbuf-2.0-0 gir1.2-poppler-0.18 gir1.2-gdkpixbuf-2.0 gir1.2-rsvg-2.0 \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*

ENV VIRTUALENV=/virtualenv
ENV UWSGI_ENGINE=threads
ENV WSGI_APP=mat_api_server.wsgi:application
